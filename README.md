Very simple REST layer for CLI application based on gin

Searches for the application config in the same directory
````shell script
go run start.go 
````

Hand over the path to the application config
````shell script
go run start.go -config=application.json
````

## application.json

- routes - path to the routes definition file
- communication - typ of communication for the request response. Current only stderr and stdout implemented. The response must be in json format:
{
    "code": HTTP-Code
    "Response": Response
}
- port - API port
- swagger - path to the swagger files - swagger docu is available in URL/swaggerui

## routes.json

- group -  optional endpoint pre path
- routes - list of endpoints
    - method - HTTP Method
    - endpoint - route endpoint
    - command  - command to execute if endpoint triggered
    - payload  - name of the command line flag for the payload
    - path - map of the name of command line flag as key and gin path config as value - the order matters
    - query - map of the name of command line flag as key and query name as value
    - output - the typ of the output to send - Current only json and yaml implemented
