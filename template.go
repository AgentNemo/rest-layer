package main

import (
	"encoding/json"
	"flag"
	"os"
)

type TemplateResponse struct {
	Code     int             `json:"code"`
	Response TemplatePayload `json:"response"`
}

type TemplatePayload struct {
	One string `json:"one"`
	Two string `json:"two"`
}

func main() {

	//name := flag.String("name", "nameTest", "nameTest")
	//query := flag.String("query", "queryTest", "queryTest")
	payloadFlag := flag.String("payload", "payloadTest", "payloadTest")
	flag.Parse()
	var payload TemplatePayload
	json.Unmarshal([]byte(*payloadFlag), &payload)

	result, _ := json.Marshal(&TemplateResponse{
		Code:     200,
		Response: payload,
	})
	os.Stderr.WriteString(string(result))
}
